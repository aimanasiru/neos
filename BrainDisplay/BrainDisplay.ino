#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <SoftwareSerial.h>
#include <Brain.h>
#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);
SoftwareSerial Bt(14, 12, false, 256);
SoftwareSerial Bt2(0, 2, false, 256);
Brain brain(Bt2);


unsigned long looptime = 0;
unsigned long FireBasetime = 0;

// replace with your FireBase API key,
#define FIREBASE_HOST "brainproject-3bd25.firebaseio.com"
#define FIREBASE_AUTH "T7ri7Da4FhA8mjmhjrByVpAGIqBk5nPTKMMm8glN"
#define WIFI_SSID "TP-LINK_0794"
#define WIFI_PASSWORD "civilwar007"

//WiFiClient client;

byte attCount = 0;
byte attStart = 0;
byte screencount = 0;
byte screen = 0;
byte Lscreen = 1;
float Irms0 = 0.0;
float Irms1 = 0.0;
float Irms2 = 0.0;
float Irms3 = 0.0;
float Att   = 0.0;
float Med   = 0.0;
bool  newData = LOW;
bool  standby = HIGH;
int   attention       = 0;
int   signalQuality   = 0;
int   pickednum       = 0;
int   meditation      = 0;
String  rely1State = "OFF";
String  rely2State = "OFF";

void setup() {
  Serial.begin(57600);
  Bt.begin(57600);
  Bt2.begin(9600);

  pinMode(D4, OUTPUT);

  lcd.init();
  lcd.backlight();
  lcd.clear();

  lcd.setCursor(2, 0);
  lcd.print(" Starting... ");

  set_cus_char(2);
  for (int i = 0; i <= 100; i += 8) {
    print_HbarL(1, i, 0, 100);
    delay(10);
  }
  set_cus_char(3);
  for (int i = 100; i >= 0; i -= 8) {
    print_HbarR(1, i, 0, 100);
    delay(10);
  }


  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WIFI_SSID);
  lcd.clear();

  lcd.setCursor(0, 0);
  lcd.print("Connecting..");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);


  bool connected = HIGH;
  FireBasetime = millis() + 10000;
  while (WiFi.status() != WL_CONNECTED) {
    set_cus_char(2);
    for (int i = 0; i <= 100; i += 8) {
      print_HbarL(1, i, 0, 100);
      delay(10);
    }
    set_cus_char(3);
    for (int i = 100; i >= 0; i -= 8) {
      print_HbarR(1, i, 0, 100);
      delay(10);
    }

    if (FireBasetime < millis()) {
      connected = LOW;
      break;
    }
    //delay(10);
    Serial.print(".");
  }

  lcd.clear();
  if (connected) {
    lcd.setCursor(0, 0);
    lcd.print("Wifi Connected");
    lcd.setCursor(1, 1);
    lcd.print("IP: ");
    lcd.print(WiFi.localIP());

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());

  } else {

    lcd.setCursor(0, 0);
    lcd.print("Wifi Error");
    lcd.setCursor(1, 1);
    lcd.print("Offfline Mode");

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }
  FireBasetime = millis() + 1000;

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);

//  while (1) {
//    Firebase.setInt("SmartBrain/Now/Sw1", 1);
//    delay(1000);
//    Firebase.setInt("SmartBrain/Now/Sw1", 0);
//    delay(1000);
//    Serial.print(Firebase.getInt("SmartBrain/Now/Sw1"));
//  }

  delay(3000);
}

void loop() {

  readSerial();
  readBt1();

  //Current sensor reset if switch Disabled;
  if (rely1State == "OFF") {
    Irms0 = 0 ;
    Irms1 = 0;
  }
  if (rely2State == "OFF") {
    Irms2 = 0;
    Irms3 = 0;
  }

  if (brain.update()) {

    //    Serial.println(brain.readErrors());
    //    Serial.println(brain.readCSV());

    signalQuality = brain.readSignalQuality();
    attention     = brain.readAttention();
    meditation    = brain.readMeditation();

    Firebase.setInt("SmartBrain/Now/Att", attention);
    Firebase.setInt("SmartBrain/Now/Med", meditation);

    if (attention > 67 ) {
      screen = 10;
      screencount = 0 ;
      if (attStart) {
        attStart = 0;
        attCount = 0;
      } else {
        attCount ++;
      }
    } else if (attention < 50) {
      if (attCount >=  5 && attCount <= 15) {
        if (rely1State == "OFF") {
          Bt.print("A");
          Firebase.setInt("SmartBrain/Now/Sw1", 1);
        } else {
          Bt.print("a");
          Firebase.setInt("SmartBrain/Now/Sw1", 0);
        }
      } else if (attCount >=  15 && attCount <= 25) {
        if (rely2State == "OFF") {
          Bt.print("B");
          Firebase.setInt("SmartBrain/Now/Sw2", 1);
        } else {
          Bt.print("b");
          Firebase.setInt("SmartBrain/Now/Sw2", 0);
        }
      }
      attCount = 0;
      attStart = 1;
    } else {
      screen = 10;
      screencount = 0 ;
    }

    Serial.print(" \n Signal = ");
    Serial.print(signalQuality);
    Serial.print(" attention = ");
    Serial.print(attention);
    Serial.print(" meditation = ");
    Serial.println(meditation);
  }

//  if (millis() > FireBasetime) {
//    FireBasetime = millis() + 500;
//    if (Firebase.getInt("SmartBrain/Now/Sw1") == 1)
//    {
//      
//    }else{
//      
//    }
//    Firebase.getInt("SmartBrain/Now/Sw1");
//  }

  static unsigned long updateTime = 0.0;
  if (updateTime < millis()) {
    updateTime = millis() + 500;

    LCD_Process();
    Bt.print("i");
  }

  if (newData) {
    newData = LOW;
    Serial.println();
    Serial.print(" 0:");
    Serial.print(Irms0);
    Serial.print(" 1:");
    Serial.print(Irms1);
    Serial.print(" 2:");
    Serial.print(Irms2);
    Serial.print(" 3:");
    Serial.println(Irms3);
    Serial.print(" R1:");
    Serial.println(rely1State);
    Serial.print(" R2:");
    Serial.println(rely2State);

    
    Firebase.setInt("SmartBrain/Now/Watt1", Irms0);
    Firebase.setInt("SmartBrain/Now/Watt2", Irms2);
  }


}

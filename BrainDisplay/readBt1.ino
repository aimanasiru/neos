void readBt1() { //
  while (Bt.available() > 0) {
    char inChar = Bt.read();

    if (inChar == '$') {
      byte count = 0;
      String Str0 = "";
      String Str1 = "";
      String Str2 = "";
      String Str3 = "";
      rely1State = "";
      rely2State = "";

      delay(10);
      while (Bt.available() > 0) {
        delay(4);
        inChar = Bt.read();
        //Serial.write(inChar);

        if (inChar == ',')  count++;
        else if (inChar == ';') break;
        else if (count == 0) Str0 += inChar;
        else if (count == 1) Str1 += inChar;
        else if (count == 2) Str2 += inChar;
        else if (count == 3) Str3 += inChar;
        else if (count == 4) rely1State += inChar;
        else if (count == 5) rely2State += inChar;
      }
      newData = HIGH;
      Irms0 = Str0.toFloat();
      Irms1 = Str1.toFloat();
      Irms2 = Str2.toFloat();
      Irms3 = Str3.toFloat();

      Irms0 = (Irms0 + Irms1) / 2;
      Irms2 = (Irms2 + Irms3) / 2;
    }
    delay(4);
  }
}

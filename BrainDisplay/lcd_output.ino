void LCD_Process() {
  switch (screen) {

    case 0:
      if (screen != Lscreen) {
        Lscreen = screen;
        lcd.clear();
        lcd.setCursor(1, 0);
        lcd.setCursor(0, 0); lcd.print("Wi-Neuro");
        lcd.setCursor(0, 1); lcd.print(" Power Socket");
      }
      break;



    case 1:
      if (screen != Lscreen) {
        Lscreen = screen;
        lcd.clear();
        set_cus_char(1);

      }
      static float tAtt = 0;
      static float tMed = 0;
      tAtt = attention;
      tMed = meditation;

      if (tAtt > 0) {

        lcd.setCursor(1, 0);
        lcd.print("Att");
        lcd.setCursor(12, 0);
        lcd.print("Med");

        print_bar2(5,  tAtt, 0, 100);
        print_bar2(6,  tAtt, 0, 100);
        print_bar2(9,  tMed, 0, 100);
        print_bar2(10,  tMed, 0, 100);

        lcd.setCursor(1, 1); lcd.print(tAtt, 0); lcd.print("%");
        lcd.setCursor(12, 1); lcd.print(tMed, 0); lcd.print("%");
      } else {
        Lscreen = 0;
        lcd.setCursor(0, 0);
        lcd.print("Sensor");
        lcd.setCursor(4, 1);
        lcd.print("Disconnected");
      }
      break;

    case 2:
      if (screen != Lscreen) {
        Lscreen = screen;
        lcd.clear();
        set_cus_char(1);
        lcd.setCursor(0, 0);
        lcd.print("Watt");
        lcd.setCursor(0, 1);
        lcd.print("Meter");
      }
      lcd.setCursor(7, 0); lcd.print(Irms0 * 100, 0);
      lcd.setCursor(7, 1); lcd.print(Irms2 * 100, 0);
      //lcd.setCursor(12, 0); lcd.print(Irms2 * 100, 0);
      //lcd.setCursor(12, 11); lcd.print(Irms3 * 100, 0);


      print_bar1(9,   0,  Irms0 * 100, 0, 80);
      //print_bar1(9,   1,  Irms1 * 100, 0, 80);
      print_bar1(11,  0,  Irms2 * 100, 0, 80);
      //print_bar1(11,  1,  Irms3 * 100, 0, 80);

      break;

    case 3:
      if (screen != Lscreen) {
        Lscreen = screen;
        lcd.clear();
        lcd.setCursor(5, 0);
        lcd.print("Switch");
      }
      lcd.setCursor(2, 1);
      lcd.print("L:" + rely1State);
      lcd.setCursor(10, 1);
      lcd.print("R:" + rely2State);
      break;

    case 10:
      if (screen != Lscreen) {
        Lscreen = screen;
        set_cus_char(2);
        lcd.clear();
      }
      lcd.setCursor(0, 0);
      lcd.print("Att. ");
      lcd.print(attention);
      lcd.print("  ");
      lcd.setCursor(10, 0);
      lcd.print("Cnt:");
      lcd.print(attCount);
      lcd.print("    ");
      print_HbarL(1, attention, 0, 100);
      break;

    default:
      screen = 0;
  }

  if (standby) {
    if (++screencount > 8) {
      screencount = 0;
      screen++;
    }
  } else {
    screencount = 0;
  }
}

void set_cus_char(int _num) {

  byte p0[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  byte p1[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF};
  byte p2[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF};
  byte p3[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF};
  byte p4[8] = {0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF};
  byte p5[8] = {0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
  byte p6[8] = {0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
  byte p7[8] = {0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
  byte p8[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

  byte b1[8] = {0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10};
  byte b2[8] = {0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18};
  byte b3[8] = {0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C};
  byte b4[8] = {0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E};
  byte b5[8] = {0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F};

  byte c1[8] = {0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01};
  byte c2[8] = {0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03};
  byte c3[8] = {0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07};
  byte c4[8] = {0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F};
  byte c5[8] = {0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F};

  //for (byte i = 1; i <= 8; i++) lcd.createChar(i, p0);

  switch (_num) {
    case 1:     // vertical down to up
      lcd.createChar(1, p1);
      lcd.createChar(2, p2);
      lcd.createChar(3, p3);
      lcd.createChar(4, p4);
      lcd.createChar(5, p5);
      lcd.createChar(6, p6);
      lcd.createChar(7, p7);
      lcd.createChar(8, p8);
      break;
    case 2:
      lcd.createChar(1, b1);
      lcd.createChar(2, b2);
      lcd.createChar(3, b3);
      lcd.createChar(4, b4);
      lcd.createChar(5, b5);
      break;
    case 3:
      lcd.createChar(1, c1);
      lcd.createChar(2, c2);
      lcd.createChar(3, c3);
      lcd.createChar(4, c4);
      lcd.createChar(5, c5);
      break;
  }
}


void print_HbarL(int _row, byte _count, int _min, int _max) {
  int _value = map( _count, _min, _max, 0, 80);

  for (byte i = 0; i < 16; i++) {
    lcd.setCursor(i, _row);
    int T_value = _value - (5 * i);

    if      (T_value >= 5) lcd.write(5);
    else if (T_value <= 0) lcd.print(" ");
    else {
      switch (T_value) {
        case 1:  lcd.write(1);   break;
        case 2:  lcd.write(2);   break;
        case 3:  lcd.write(3);   break;
        case 4:  lcd.write(4);   break;
        case 5:  lcd.write(5);   break;
        default: lcd.print(" ");  break;
      }
    }
  }
}

void print_HbarR(int _row, byte _count, int _min, int _max) {
  int _value = map( _count, _min, _max, 80, 0);

  for (int i = 15; i >= 0; i--) {
    lcd.setCursor(i, _row);
    int T_value = _value - (5 * i);

    if      (T_value >= 5) lcd.print(" ");
    else if (T_value <= 0) lcd.write(5);
    else {
      switch (T_value) {
        case 1:  lcd.write(5);   break;
        case 2:  lcd.write(4);   break;
        case 3:  lcd.write(3);   break;
        case 4:  lcd.write(2);   break;
        case 5:  lcd.write(1);   break;
        default: lcd.print(" ");  break;
      }
    }
  }
}


void print_bar1(byte _col, byte _row, byte _count, int _min, int _max) {

  byte _value = map( _count, _min, _max, 0, 8);

  lcd.setCursor(_col, _row);
  switch (_value) {
    case 0: lcd.print(" "); break;
    case 1: lcd.write(1); break;
    case 2: lcd.write(2); break;
    case 3: lcd.write(3); break;
    case 4: lcd.write(4); break;
    case 5: lcd.write(5); break;
    case 6: lcd.write(6); break;
    case 7: lcd.write(7); break;
    case 8: lcd.write(8); break;
    default: lcd.write(8); break;
  }

}

void print_bar2(byte _col, byte _count, int _min, int _max) {

  byte _value = map( _count, _min, _max, 0, 16);

  lcd.setCursor(_col, 1);
  switch (_value) {
    case 0: lcd.print(" "); break;
    case 1: lcd.write(1); break;
    case 2: lcd.write(2); break;
    case 3: lcd.write(3); break;
    case 4: lcd.write(4); break;
    case 5: lcd.write(5); break;
    case 6: lcd.write(6); break;
    case 7: lcd.write(7); break;
    case 8: lcd.write(8); break;
    default: lcd.write(8); break;
  }
  lcd.setCursor(_col, 0);
  switch (_value) {
    case 9: lcd.write(1); break;
    case 10: lcd.write(2); break;
    case 11: lcd.write(3); break;
    case 12: lcd.write(4); break;
    case 13: lcd.write(5); break;
    case 14: lcd.write(6); break;
    case 15: lcd.write(7); break;
    case 16: lcd.write(8); break;
    default: lcd.print(" "); break;
  }
}

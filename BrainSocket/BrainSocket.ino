// EmonLibrary examples openenergymonitor.org, Licence GNU GPL V3

#include "EmonLib.h"                   // Include Emon Library
#include <SoftwareSerial.h>

#define relayPin0 7
#define relayPin1 8
#define led0      5
#define led1      6

SoftwareSerial Bt(10, 11); // RX, TX
EnergyMonitor emon0;                   // Create an instance
EnergyMonitor emon1;                   // Create an instance
EnergyMonitor emon2;                   // Create an instance
EnergyMonitor emon3;                   // Create an instance

unsigned long lastUpdate = 0;
double Irms0 = 0.0;
double Irms1 = 0.0;
double Irms2 = 0.0;
double Irms3 = 0.0;
bool  relayState0 = 0;
bool  relayState1 = 0;
bool  relayLState0 = 0;
bool  relayLState1 = 0;

void setup()
{
  Serial.begin(57600);
  Bt.begin(57600);

  pinMode(relayPin0 , OUTPUT);
  pinMode(relayPin1 , OUTPUT);
  pinMode(led0 , OUTPUT);
  pinMode(led1 , OUTPUT);
  digitalWrite(relayPin0, LOW);
  digitalWrite(relayPin1, LOW);
  digitalWrite(led0, LOW);
  digitalWrite(led1, LOW);

  emon0.current(0, 8.607);             // Current: input pin, calibration.
  emon1.current(1, 14.18);             // Current: input pin, calibration.
  emon2.current(2, 8.607);             // Current: input pin, calibration.
  emon3.current(3, 14.18);             // Current: input pin, calibration.

}

void loop()
{

  while ( Bt.available() > 0 ) {
    char inChar = Bt.read();

    if ( inChar == 'i' && millis() > lastUpdate ) {
      lastUpdate = millis() + 500;

      Bt.print ("$");
      Bt.print (Irms0);
      Bt.print (",");
      Bt.print (Irms1);
      Bt.print (",");
      Bt.print (Irms2);
      Bt.print (",");
      Bt.print (Irms3);
      Bt.print (",");
      if (relayState0 == HIGH) Bt.print ("ON");
      else                      Bt.print ("OFF");
      Bt.print (",");
      if (relayState1 == HIGH) Bt.print ("ON");
      else                      Bt.print ("OFF");
      Bt.print (";");
    }

    if (inChar == 'A')  relayState0 = HIGH;
    if (inChar == 'a')  relayState0 = LOW;
    if (inChar == 'B')  relayState1 = HIGH;
    if (inChar == 'b')  relayState1 = LOW;

  }

  if (relayState0 != relayLState0) {
    relayLState0 = relayState0;
    digitalWrite(relayPin0, relayState0);
    digitalWrite(led0, relayState0);
  }
  if (relayState1 != relayLState1) {
    relayLState1 = relayState1;
    digitalWrite(relayPin1, relayState1);
    digitalWrite(led1, relayState1);
  }


  Irms0 = emon0.calcIrms(1480);  // Calculate Irms only
  Irms1 = emon1.calcIrms(1480);  // Calculate Irms only
  Irms2 = emon2.calcIrms(1480);  // Calculate Irms only
  Irms3 = emon3.calcIrms(1480);  // Calculate Irms only

  Serial.print(Irms0, 4);		      // Irms
  Serial.print("  ");          // Irms
  Serial.print(Irms1, 4);          // Irms
  Serial.print(" ||| ");
  Serial.print(Irms0 * 230, 4);        // Irms
  Serial.print("  ");          // Irms
  Serial.println(Irms1 * 230, 4);        // Irms
}
